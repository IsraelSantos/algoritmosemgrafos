#include "heap.h"

heap :: heap()
{
    //ctor
}

heap :: ~heap()
{
    //dtor
}

int heap :: filhoEsq(int i){
    return 2*i;
}

int heap :: filhoDir(int i){
    return 2*i+1;
}

int heap :: pai(int i){
    return i/2;
}

void heap :: heapify(int i){//Optei por implementar a minha pr�pria fila de prioridade porque stl::priority_queue n�o tinha esse m�todo p�blico.
    int n=v.size();
    int e, d , m;
    e=filhoEsq(i);
    d=filhoDir(i);
    if (e<n && v[e]->getChave()<v[i]->getChave()){
        m = e;
    }else
        m = i;
    if (d<n && v[d]->getChave()<v[m]->getChave()){
            m = d;
    }
    if (m != i){
        vertice* tmp=v[i];
        v[i]=v[m];
        v[m]=tmp;
        v[m]->setIdHeap(m);
        v[i]->setIdHeap(i);
        heapify(m);
    }
}

void heap :: heapiup(int i){//Retorna para a raiz ajeitando o heap
    while (i>0 && v[pai(i)]->getChave() > v[i]->getChave()){
        vertice* tmp = v[pai(i)];
        v[pai(i)] = v[i];
        v[i]=tmp;
        v[pai(i)]->setIdHeap(pai(i));
        v[i]->setIdHeap(i);
        i=pai(i);
    }
}

void heap :: push(vertice* data){
    v.push_back(data);
    int i=v.size()-1;
    data->setIdHeap(i);
    heapiup(i);
}

vertice* heap :: top(){
    return v[0];
}

void heap :: pop(){
    v[0]=v[v.size()-1];
    v[0]->setIdHeap(0);
    v.pop_back();
    heapify(0);
}

bool heap :: empty(){
    return v.size()==0;
}

void heap :: manter_heap(int a){
    if (v[pai(a)]->getChave() > v[a]->getChave())
        heapiup(a);//ou sobe
    else
        heapify(a);//ou desce
}

bool heap :: pertence(vertice *a){
    for (int i=0; i<v.size(); i++){
        if (v[i]==a)
            return true;
    }
    return false;
}
