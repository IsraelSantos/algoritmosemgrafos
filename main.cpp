#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Grafo.h"


using namespace std;

int main(int argc, char *argv[])
{
    bool exe=true;
    Grafo *grafo=NULL;
    while (exe){
        system("clear");
        cout<<"Para ler arquivo digite:\n read <arquivo.g>\n\n"<<
                "Para percurso em largura digite:\n bl <vertice inicial>\n\n"<<
                "Para percurso em profundidade digite:\n bp <vertice inicial>\n\n"<<
                "Para ordenacao topologica digite:\n ot\n\n"<<
                "Para calculo de uma arvore geradora minima digite:\n agm <arquivosaida.g>\n\n"<<
                "Para caminho minimo entre um par de vertices digite:\n sp <vertice inicial> <vertice final>\n\n"<<
                "Para sair digite:\n sair\n\n$ ";
        char vet[3][100];
        for (int i=0; i<3; i++){
            cin>>vet[i];
            if ((strcmp(vet[0],"sp")==-1 && i==1) || ((strcmp(vet[0],"ot")==0 || strcmp(vet[0],"sair")==0) && i==0))
                break;

        }
        if (strcmp(vet[0], "read")==0){
            if (grafo != NULL)
                delete grafo;
            grafo=new Grafo(vet[1]);
        }else{
            if (grafo != NULL){
                if (strcmp(vet[0], "bl")==0){
                    int ver=atoi(vet[1]);
                    grafo->buscaLargura(grafo->getVertice(ver));
                    cout<<"\nPercurso feito.\n";
                    //system("sleep 6");
                }else{
                    if (strcmp(vet[0], "bp")==0){
                        int ver=atoi(vet[1]);
                        grafo->buscaEmProfundidade(&grafo->getVertice(ver), true);
                        cout<<"\nPercurso feito.\n";
                        //system("sleep 6");
                    }else{
                        if (strcmp(vet[0], "ot")==0){
                            grafo->ordenacaoTopologica();
                            cout<<"\nOrdenacao feita.\n";
                            //system("sleep 6");
                        }else{
                            if (strcmp(vet[0], "agm")==0){
                                Grafo *a=grafo->prim(grafo->getVertice(1));
                                cout<<"A arvore geradora eh:"<<endl;
                                a->imprimeEmArquivo(vet[1]);
                                cout<<"\nArvore impressa em arquivo.\n";
                                //system("sleep 2");
                            }else{
                                if (strcmp(vet[0], "sp")==0){
                                    int ver1=atoi(vet[1]);
                                    int ver2=atoi(vet[2]);
                                    grafo->bellmanFord(grafo->getVertice(ver1));
                                    grafo->imprimePeso(ver1,ver2);
                                    grafo->imprimeCaminho(ver1, ver2);
                                    //system("sleep 2");
                                }else{
					if (strcmp(vet[0], "sair")==0){
						delete grafo;
                    				break;
					}else cout<<"Parametro invalido!\n";
				}
                            }
                        }
                    }
                }
            }else{
                if (strcmp(vet[0], "sair")==0)
                    break;
                cout<<"Voce deve fazer o programa ler o grafo antes de executar outros comandos!\n\n";
            }
        }
        if (exe){
            cout<<"\nDigite 1 para continuar ou 0 para fechar: ";
            int val;
            cin>>val;
            if (val==0)
                exe=false;
        }
    }

    return 0;
}
