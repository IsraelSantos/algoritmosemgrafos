#ifndef HEAP_H
#define HEAP_H
#include "vertice.h"

class heap
{
    private:
        vector<vertice*> v;
        int filhoEsq(int i);
        int filhoDir(int i);
        int pai(int i);
        void heapify(int i);
        void heapiup(int i);
    public:
        heap();
        ~heap();
        void push(vertice* data);
        vertice* top();
        void pop();
        bool empty();
        void manter_heap(int a);
        bool pertence(vertice *a);
};

#endif // HEAP_H
