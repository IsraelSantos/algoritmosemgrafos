#include "vertice.h"

vertice::vertice()
{

}

vertice::~vertice()
{

}

void vertice :: addAresta(aresta &a){
    this->vizin.push_back(a);
}

vector<aresta>& vertice :: getVizin(){
    return this->vizin;
}

void vertice :: setD(float d){
    this->d = d;
}

float vertice :: getD(){
    return d;
}

void vertice :: setF(float f){
    this->f = f;
}

float vertice :: getF(){
    return f;
}

void vertice :: setCor(cor c){
    this->c = c;
}

cor vertice :: getCor(){
    return c;
}

void vertice :: setPi(vertice &pi){
    this->pi = &pi;
}

vertice* vertice :: getPi(){
    return this->pi;
}

void vertice :: setVertice(int vert){
    this->vert = vert;
}

int vertice :: getVert(){
    return this->vert;
}

int vertice :: getIdHeap(){
    return this->idHeap;
}

void vertice :: setIdHeap(int id){
    this->idHeap = id;
}

int vertice :: getChave(){
    return this->chave;
}

void vertice :: setChave(int ch){
    this->chave=ch;
}

bool vertice :: existeArestaPara(int k){
    for (int i=0; i<this->vizin.size(); i++){
        if (this->vizin[i].getProxVert() == k)
            return true;
    }
    return false;
}

bool vertice :: operator>(vertice a) const{
    return (this->f > a.getF())? true : false;
}

bool vertice :: operator<(vertice a) const{
    return (this->f < a.getF())? true : false;
}
