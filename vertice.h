#ifndef VERTICE_H
#define VERTICE_H

#include <vector>
#include "aresta.h"

using namespace std;

enum cor{ //Enumerator contendo as cores poss�veis para cada v�rtice
    BRANCO,
    PRETO,
    CINZA
};

class vertice
{
    private:
        vector<aresta> vizin;
        int vert; //Valor/indice do vertice
        float d;
        float f;
        cor c;
        vertice *pi;
        float chave;
        int idHeap;//Guarda o �ndice do vertice no heap para a chamada do m�todo heapify(idHeap) (MANTEM_HEAP(v)).
    public:
        vertice();
        virtual ~vertice();
        bool operator ==(vertice a);
        void addAresta(aresta &a);
        vector<aresta>& getVizin();
        void setD(float d);
        float getD();
        void setF(float f);
        float getF();
        void setCor(cor c);
        cor getCor();
        void setPi(vertice &pi);
        vertice* getPi();
        int getVert();
        void setVertice(int vert);
        int getChave();
        void setChave(int ch);
        int getIdHeap();
        void setIdHeap(int id);
        bool existeArestaPara(int i);
        bool operator>(vertice a) const;
        bool operator<(vertice a) const;
    protected:

};

#endif // VERTICE_H
