#ifndef ARESTA_H
#define ARESTA_H

//#include "vertice.h"


class aresta
{
    public:
        aresta(int proxVert, float peso);
        virtual ~aresta();
        float getPeso();
        int getProxVert();
    protected:
    private:
        int proxVert;
        float peso;
};

#endif // ARESTA_H
