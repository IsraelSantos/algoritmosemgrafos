#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include <fstream>
#include <vector>
#include "vertice.h"
#include "aresta.h"
#include "heap.h"
#include <queue>

using namespace std;

class Grafo
{
    private:
        vertice *v;
        int length;
        int tempo;
        float pesoCaminho;

        void inicializaBellmanFord(vertice &s);
        void relaxaBellmanFord(int w, int u);
        void inicializaBuscaLargura(vertice &s);
        void visitaEmProfundidade(vertice &s, bool mostra);
        float peso(int a, int b);
        vertice* ordenaF();
        float getPeso();

    public:
        Grafo(int num_vert); //Construtor importante para a constru��o da �rvore geradora m�nima.
        Grafo(char *file_name);
        void mostrar();
        virtual ~Grafo();
        vertice& getVertice(int x);
        void bellmanFord(vertice &s);
        void buscaLargura(vertice &s);
        void buscaEmProfundidade(vertice *u, bool mostra);
        void imprimeCaminho(int w, int u);
        float imprimePeso(int w, int u);
        void imprimeEmArquivo(char *file_name);
        void ordenacaoTopologica();
        Grafo* prim(vertice &s);

    protected:


};

#endif // GRAFO_H
