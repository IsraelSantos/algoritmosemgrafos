#include "aresta.h"

aresta::aresta(int proxVert, float peso)
{
    this->proxVert = proxVert;
    this->peso = peso;
}

aresta::~aresta()
{

}

float aresta :: getPeso()
{
    return this->peso;
}

int aresta :: getProxVert()
{
    return this->proxVert;
}

