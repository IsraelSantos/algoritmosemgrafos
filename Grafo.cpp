#include "Grafo.h"
#include <queue>

//------------------Construtor padr�o do grafo com leitura do arquivo-------------------------
Grafo::Grafo(char *file_name)
{
    try{
        ofstream teste;
        teste.open(file_name, ios_base::in);
        if(!teste){
            throw "ERRO: O arquivo especificado nao existe no diretorio.";
        }else{
            try{
                ifstream in(file_name);
                if (!in.eof()){
                    in>>this->length;
                    this->v=new vertice[this->length];
                    for (int i=0; i<this-> length; i++){
                        v[i].setVertice(i);
                    }

                    while (!in.eof()){
                        int i,j,w;
                        in>>i>>j>>w;
                        aresta *a=new aresta(j-1,w);
                        v[i-1].addAresta((*a));
                    }

                }
             cout << "Grafo lido.\n";
             }catch(const char *msg){
                cout<<msg<<"\n";
            }
        }
    }catch(const char *msg){
        cout<<msg<<"\n";
    }
    this->pesoCaminho = 0;
}

Grafo::Grafo(int num_vert)//Construtor importante para a constru��o da �rvore geradora m�nima.
{
    this->length = num_vert;
    this->v=new vertice[this->length];
    for (int i=0; i<this-> length; i++){
        v[i].setVertice(i);
    }
    this->pesoCaminho = 0;
}

float Grafo :: getPeso()
{
    return this->pesoCaminho;
}

void Grafo::mostrar()//Imprime o grafo na tela
{
    for (int i=0; i<this->length ; i++)
    {
        vector<aresta> a=(this->v[i]).getVizin();
        cout<<"Vertice("<<i+1<<"):\t";
        for (int j=0; j<a.size(); j++){
            cout<<"("<<a[j].getProxVert()+1<<", "<<a[j].getPeso()<<")\t";
        }
        cout<<"\n";

    }
}

void Grafo :: imprimeEmArquivo(char *file_name) //imprime um grafo em arquivo .g
{
    ofstream out(file_name);
    out<<this->length<<endl;
    cout<<this->length<<endl;
    for (int i=0; i<this->length; i++){
        vector<aresta> viz=this->v[i].getVizin();
        for (int j=0; j<viz.size(); j++){
            out<<i+1<<" "<<viz[j].getProxVert()+1<<" "<<viz[j].getPeso()<<endl;
            cout<<i+1<<" "<<viz[j].getProxVert()+1<<" "<<viz[j].getPeso()<<endl;
        }
    }
    out.close();
}

Grafo::~Grafo()
{
    delete [] v;
}

vertice& Grafo :: getVertice(int x)
{
    try{
        if(x<1 || x>this->length)
            throw "ERRO: O vertice passado nao pertence a V(G)";
        return this->v[x-1];
    } catch(const char *msg){
        cout<<msg<<"\n";
    }
}

//------------------Caminho M�nimo usando Bellman-Ford-----------------------------

void Grafo :: inicializaBellmanFord(vertice &s) //inicializa
{
    vertice *c = NULL;
    for (int i=0; i<this->length; i++){
        this->v[i].setD(99999);
        this->v[i].setPi(*c);
    }
    s.setD(0);
}

void Grafo :: relaxaBellmanFord(int w, int u) //Relaxa
{
    vector<aresta> a = (this->v[w]).getVizin();
    float peso = a[u].getPeso();
    vertice *w1 = &(this->v[w]);
    vertice *u1 = &(this->v[(a[u].getProxVert())]);

    if(w1->getD() > (u1->getD() + peso)){
        w1->setD(u1->getD()+peso);
        w1->setPi(*u1);
    }
}

void Grafo :: bellmanFord(vertice &s)
{
    cout<<"------------------Caminho Minimo----------------\n"<<endl;
    inicializaBellmanFord(s);
    for(int k=0;k<this->length-1;k++){
        for(int i=0;i<this->length;i++){
            vector<aresta> a = (this->v[i]).getVizin();
            for(int j=0;j<a.size();j++){
                relaxaBellmanFord(i, j);
            }
        }
    }

    vertice *w1, *u1;

    for(int i=0;i<this->length;i++){
        vector<aresta> a = (this->v[i]).getVizin();
        for(int j=0;j<a.size();j++){
            vector<aresta> b = (this->v[i]).getVizin();
            float peso = b[j].getPeso();
            w1 = &this->v[i];
            u1 = &this->v[(b[j].getProxVert())];

            if(w1->getD() > (u1->getD() + peso)){
                cout << "Existem ciclos negativos.\n";
                return ;
            }
        }
    }
    cout << "Caminhos minimos calculados\n\n";
}

float Grafo :: imprimePeso(int w, int u){ //Imprime o peso do caminho de w a u
    if(w==u){
        this->pesoCaminho=0;
    }else{
        do{
            vector<aresta> x = (this->v[u-1]).getVizin();
            int ve = (this->v[u-1].getPi())->getVert();
            for(int i=0;i<x.size();i++){
                if(x[i].getProxVert()==ve){
                    (this->pesoCaminho)+=(this->v[u-1]).getVizin()[i].getPeso();
                }
            }
            u=(this->v[u-1].getPi())->getVert()+1;

        }while(u!=w);
    }
    cout << this->pesoCaminho << "\n";
    this->pesoCaminho = 0;
}

void Grafo :: imprimeCaminho(int w, int u) //Imprime o camimho minimo de w a u
{
    if (w==u)
        cout<<w<<" ";
    else{
        vertice *ve = &(this->v[u-1]);
        if ((*ve).getPi()==NULL)
            cout<<"Nao existe caminho de "<<w<<" a "<<u<<"!"<<endl;
        else{
            vertice *a=ve->getPi();
            imprimeCaminho(w, a->getVert()+1);
            cout<<"-> "<<u<<" ";
        }
    }
}

//------------------Busca em Largura-----------------------------

void Grafo :: inicializaBuscaLargura(vertice &s){
    vertice *c = NULL;
    for (int i=0; i<this->length; i++){
        v[i].setCor(BRANCO);
        v[i].setD(99999);
        v[i].setPi(*c);
    }
    s.setCor(CINZA);
    s.setPi(*c);
}

void Grafo :: buscaLargura(vertice &s)
{
    cout<<"\n------------------Busca em Largura----------------"<<endl;
    inicializaBuscaLargura(s);
    queue<vertice> q;
    q.push(s);
    cout<<s.getVert()+1;
    while(!q.empty()){
        int vert= q.front().getVert();
        vertice *u = &(this->v[vert]);
        q.pop();
        vector<aresta> viz=u->getVizin();
        for (int i=0; i< viz.size(); i++){
            vertice *vad=&(this->v[viz[i].getProxVert()]);
            if (vad->getCor() == BRANCO){
                vad->setCor(CINZA);
                vad->setD(u->getD()+1);
                vad->setPi(*u);
                cout<<" "<<viz[i].getProxVert()+1;
                q.push(*vad);
            }
        }
        u->setCor(PRETO);
    }
}

//------------------Busca em Profundidade-------------------------

void Grafo :: visitaEmProfundidade(vertice &s, bool mostra)
{
    if (mostra)
        cout << s.getVert()+1<<" ";
    s.setCor(CINZA);
    this->tempo++;
    s.setD(this->tempo);
    vector<aresta> viz=s.getVizin();
    for (int i=0; i<viz.size(); i++){
        vertice *vad=&(this->v[viz[i].getProxVert()]);
        if (vad->getCor() == BRANCO){
            vad->setPi(s);
            visitaEmProfundidade(*vad, mostra);
        }
    }
    s.setCor(PRETO);
    this->tempo++;
    s.setF(this->tempo);
}

void Grafo :: buscaEmProfundidade(vertice *u, bool mostra)
{
    if (mostra)
        cout<<"\n----------------Busca em Profundidade--------------"<<endl;
    //Inicializa��o
    vertice *c = NULL;
    for (int i=0; i<this->length; i++){
        v[i].setCor(BRANCO);
        v[i].setPi(*c);
    }
    this->tempo=0;
    //Se u for diferente de nulo executa apenas o visitaEmProfundidade(u) como pedido no trabalho.
    if (u == NULL){//Se n�o quer mostrar apenas os v�rtices que podem ser alcan�ados a partir do v�rtice que � escolhido pelo usu�rio como v�rtice inicial.
        for (int j=0; j<this->length; j++){
            if (v[j].getCor() == BRANCO){
                visitaEmProfundidade(v[j], mostra);
            }
        }
    }else visitaEmProfundidade(*u, mostra);
}

//------------------Ordena��o topol�gica-------------------------
void Grafo :: ordenacaoTopologica()
{
    buscaEmProfundidade(NULL, false);
    vertice *v=ordenaF();
    cout <<"\n------------------Ordenacao Topologica----------------\n";
    for(int i=0;i<this->length;i++){
        cout << v[i].getVert()+1 << " ";
    }
}

vertice* Grafo :: ordenaF(){ //Ordena os vertices em rela��o o valor de F
    vertice *y=new vertice[this->length];
    priority_queue<vertice> a;
    for (int i=0; i < this->length; i++){
        a.push(this->v[i]);
    }
    for (int i=0; i < this->length; i++){
        y[i]=a.top();
        a.pop();
    }
    return y;
}

//------------------�rvore geradora m�nima---------------------

float Grafo :: peso(int a, int b){//Retorna o peso da aresta do vertice a para o b se ela existir.
    vertice *ve = &(this->v[a-1]);
    vector<aresta> viz=ve->getVizin();
    for (int i=0; i<viz.size(); i++){
        if (viz[i].getProxVert() == b-1)
            return viz[i].getPeso();
    }
    throw "A aresta n�o exista.";
}

Grafo* Grafo :: prim(vertice &s)
{
    vertice *c = NULL;
    for (int i=0; i<this->length; i++){
        v[i].setChave(99999);
        v[i].setPi(*c);
    }
    s.setChave(0);
    heap q;
    for (int i=0; i<this->length; i++){
        q.push(&this->v[i]);
    }
    while(!q.empty()){
        vertice *u = q.top();
        q.pop();
        vector<aresta> viz=u->getVizin();
        for (int i=0; i<viz.size(); i++){
            vertice *vad=&(this->v[viz[i].getProxVert()]);
            if (q.pertence(vad) && viz[i].getPeso() < vad->getChave()){
                vad->setPi(*u);
                vad->setChave(viz[i].getPeso());
                q.manter_heap(vad->getIdHeap());
            }
        }
    }
    float custoGrafo = 0;
    //Gerando um novo grafo (a �rvore geradora). Este trecho n�o faz parte do algoritmo do prim.
    Grafo *res=new Grafo(this->length);
    for (int i=0; i<this->length; i++){
        if (this->v[i].getPi() != NULL){
            vertice *pi = this->v[i].getPi();
            float pesotmp=peso(pi->getVert()+1, i+1);
            aresta *a=new aresta(i,pesotmp);
            (res->getVertice(pi->getVert()+1)).addAresta(*a);
            aresta *a2=new aresta(pi->getVert(),pesotmp);
            (res->getVertice(i+1)).addAresta(*a2);
            custoGrafo += pesotmp;
        }
    }
    cout<<"O custo da arvore geradora minima eh: "<<custoGrafo<<endl;
    return res;
}


